﻿
namespace WINStreamOnVisionAR
{
    partial class WindowSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.KryptonListBoxWindows = new ComponentFactory.Krypton.Toolkit.KryptonListBox();
            this.SuspendLayout();
            // 
            // KryptonListBoxWindows
            // 
            this.KryptonListBoxWindows.FormattingEnabled = true;
            this.KryptonListBoxWindows.HorizontalExtent = 600;
            this.KryptonListBoxWindows.HorizontalScrollbar = true;
            this.KryptonListBoxWindows.Location = new System.Drawing.Point(24, 12);
            this.KryptonListBoxWindows.Name = "KryptonListBoxWindows";
            this.KryptonListBoxWindows.Size = new System.Drawing.Size(187, 285);
            this.KryptonListBoxWindows.TabIndex = 0;
            this.KryptonListBoxWindows.ListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.KryptonListBoxWindows_MouseDoubleClick);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(235, 309);
            this.Controls.Add(this.KryptonListBoxWindows);
            this.Name = "Windows";
            this.Text = "Windows";
            this.ResumeLayout(false);
        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonListBox KryptonListBoxWindows;
    }
}
