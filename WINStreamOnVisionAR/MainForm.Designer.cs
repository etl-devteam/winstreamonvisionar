﻿namespace WINStreamOnVisionAR
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.KryptonNavigator = new ComponentFactory.Krypton.Navigator.KryptonNavigator();
            this.KryptonPageVideo = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonCheckBoxInvertColourVideo = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.KryptonGroupBoxVideo = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonButtonPlayPause = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonStop = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonLabelVideoName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonButtonBrowseVideoFile = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonTextBoxVideoFile = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.PictureBoxVideo = new System.Windows.Forms.PictureBox();
            this.KryptonPageWindow = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonCheckBoxInvertColourStream = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.KryptonGroupBoxStream = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonButtonStartStreaming = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonStopStreaming = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonLabelForegroundWindowMessage = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.PictureBoxWindowStream = new System.Windows.Forms.PictureBox();
            this.KryptonButtonBrowseWindows = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonLabelWindowName = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.KryptonTextBoxWindowName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.KryptonPageAreaStream = new ComponentFactory.Krypton.Navigator.KryptonPage();
            this.KryptonGroupBoxArea = new ComponentFactory.Krypton.Toolkit.KryptonGroupBox();
            this.KryptonButtonDoneAreaStreaming = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonButtonStartAreaStreaming = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.KryptonCheckBoxInvertColour = new ComponentFactory.Krypton.Toolkit.KryptonCheckBox();
            this.PictureBoxAreaStreaming = new System.Windows.Forms.PictureBox();
            this.TimerWindowStreaming = new System.Windows.Forms.Timer(this.components);
            this.TimerAreaStreaming = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonNavigator)).BeginInit();
            this.KryptonNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageVideo)).BeginInit();
            this.KryptonPageVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVideo.Panel)).BeginInit();
            this.KryptonGroupBoxVideo.Panel.SuspendLayout();
            this.KryptonGroupBoxVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxVideo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageWindow)).BeginInit();
            this.KryptonPageWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxStream)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxStream.Panel)).BeginInit();
            this.KryptonGroupBoxStream.Panel.SuspendLayout();
            this.KryptonGroupBoxStream.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWindowStream)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageAreaStream)).BeginInit();
            this.KryptonPageAreaStream.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxArea.Panel)).BeginInit();
            this.KryptonGroupBoxArea.Panel.SuspendLayout();
            this.KryptonGroupBoxArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAreaStreaming)).BeginInit();
            this.SuspendLayout();
            // 
            // KryptonNavigator
            // 
            this.KryptonNavigator.Bar.BarMapExtraText = ComponentFactory.Krypton.Navigator.MapKryptonPageText.Title;
            this.KryptonNavigator.Bar.BarMapText = ComponentFactory.Krypton.Navigator.MapKryptonPageText.Text;
            this.KryptonNavigator.Bar.BarOrientation = ComponentFactory.Krypton.Toolkit.VisualOrientation.Left;
            this.KryptonNavigator.Bar.CheckButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.LowProfile;
            this.KryptonNavigator.Bar.ItemOrientation = ComponentFactory.Krypton.Toolkit.ButtonOrientation.FixedTop;
            this.KryptonNavigator.Bar.TabBorderStyle = ComponentFactory.Krypton.Toolkit.TabBorderStyle.OneNote;
            this.KryptonNavigator.Button.ButtonDisplayLogic = ComponentFactory.Krypton.Navigator.ButtonDisplayLogic.None;
            this.KryptonNavigator.Button.CloseButtonDisplay = ComponentFactory.Krypton.Navigator.ButtonDisplay.Hide;
            this.KryptonNavigator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.KryptonNavigator.Location = new System.Drawing.Point(0, 0);
            this.KryptonNavigator.Name = "KryptonNavigator";
            this.KryptonNavigator.NavigatorMode = ComponentFactory.Krypton.Navigator.NavigatorMode.BarCheckButtonGroupOutside;
            this.KryptonNavigator.PageBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelClient;
            this.KryptonNavigator.Pages.AddRange(new ComponentFactory.Krypton.Navigator.KryptonPage[] {
            this.KryptonPageVideo,
            this.KryptonPageWindow,
            this.KryptonPageAreaStream});
            this.KryptonNavigator.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonNavigator.Panel.PanelBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.PanelAlternate;
            this.KryptonNavigator.SelectedIndex = 0;
            this.KryptonNavigator.Size = new System.Drawing.Size(800, 450);
            this.KryptonNavigator.StateCommon.Bar.BarPaddingOutside = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.KryptonNavigator.StateCommon.Bar.CheckButtonGap = 5;
            this.KryptonNavigator.StateCommon.CheckButton.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.KryptonNavigator.StateCommon.CheckButton.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.KryptonNavigator.StateCommon.CheckButton.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KryptonNavigator.TabIndex = 85;
            this.KryptonNavigator.Text = "KryptonNavigatorMain";
            // 
            // KryptonPageVideo
            // 
            this.KryptonPageVideo.AutoHiddenSlideSize = new System.Drawing.Size(150, 150);
            this.KryptonPageVideo.Controls.Add(this.KryptonCheckBoxInvertColourVideo);
            this.KryptonPageVideo.Controls.Add(this.KryptonGroupBoxVideo);
            this.KryptonPageVideo.Controls.Add(this.KryptonLabelVideoName);
            this.KryptonPageVideo.Controls.Add(this.KryptonButtonBrowseVideoFile);
            this.KryptonPageVideo.Controls.Add(this.KryptonTextBoxVideoFile);
            this.KryptonPageVideo.Controls.Add(this.PictureBoxVideo);
            this.KryptonPageVideo.Flags = 62;
            this.KryptonPageVideo.LastVisibleSet = true;
            this.KryptonPageVideo.MinimumSize = new System.Drawing.Size(50, 50);
            this.KryptonPageVideo.Name = "KryptonPageVideo";
            this.KryptonPageVideo.Size = new System.Drawing.Size(656, 448);
            this.KryptonPageVideo.Text = "Video Stream";
            this.KryptonPageVideo.TextDescription = "";
            this.KryptonPageVideo.TextTitle = "";
            this.KryptonPageVideo.ToolTipTitle = "Page ToolTip";
            this.KryptonPageVideo.UniqueName = "F1890C23D1634D34F1890C23D1634D34";
            // 
            // KryptonCheckBoxInvertColourVideo
            // 
            this.KryptonCheckBoxInvertColourVideo.Location = new System.Drawing.Point(281, 209);
            this.KryptonCheckBoxInvertColourVideo.Name = "KryptonCheckBoxInvertColourVideo";
            this.KryptonCheckBoxInvertColourVideo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonCheckBoxInvertColourVideo.Size = new System.Drawing.Size(94, 20);
            this.KryptonCheckBoxInvertColourVideo.TabIndex = 8;
            this.KryptonCheckBoxInvertColourVideo.Values.Text = "Invert Colour";
            // 
            // KryptonGroupBoxVideo
            // 
            this.KryptonGroupBoxVideo.Location = new System.Drawing.Point(177, 303);
            this.KryptonGroupBoxVideo.Name = "KryptonGroupBoxVideo";
            this.KryptonGroupBoxVideo.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxVideo.Panel
            // 
            this.KryptonGroupBoxVideo.Panel.Controls.Add(this.KryptonButtonPlayPause);
            this.KryptonGroupBoxVideo.Panel.Controls.Add(this.KryptonButtonStop);
            this.KryptonGroupBoxVideo.Size = new System.Drawing.Size(283, 111);
            this.KryptonGroupBoxVideo.TabIndex = 6;
            this.KryptonGroupBoxVideo.Values.Heading = "Controls";
            // 
            // KryptonButtonPlayPause
            // 
            this.KryptonButtonPlayPause.Location = new System.Drawing.Point(35, 31);
            this.KryptonButtonPlayPause.Name = "KryptonButtonPlayPause";
            this.KryptonButtonPlayPause.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonPlayPause.TabIndex = 3;
            this.KryptonButtonPlayPause.Values.Text = "Play";
            this.KryptonButtonPlayPause.Click += new System.EventHandler(this.KryptonButtonPlayPause_Click);
            // 
            // KryptonButtonStop
            // 
            this.KryptonButtonStop.Enabled = false;
            this.KryptonButtonStop.Location = new System.Drawing.Point(184, 31);
            this.KryptonButtonStop.Name = "KryptonButtonStop";
            this.KryptonButtonStop.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonStop.TabIndex = 4;
            this.KryptonButtonStop.Values.Text = "Stop";
            this.KryptonButtonStop.Click += new System.EventHandler(this.KryptonButtonStop_Click);
            // 
            // KryptonLabelVideoName
            // 
            this.KryptonLabelVideoName.Location = new System.Drawing.Point(14, 247);
            this.KryptonLabelVideoName.Name = "KryptonLabelVideoName";
            this.KryptonLabelVideoName.Size = new System.Drawing.Size(93, 20);
            this.KryptonLabelVideoName.TabIndex = 5;
            this.KryptonLabelVideoName.Values.Text = "Video MP4 file:";
            // 
            // KryptonButtonBrowseVideoFile
            // 
            this.KryptonButtonBrowseVideoFile.Location = new System.Drawing.Point(481, 247);
            this.KryptonButtonBrowseVideoFile.Name = "KryptonButtonBrowseVideoFile";
            this.KryptonButtonBrowseVideoFile.Size = new System.Drawing.Size(53, 25);
            this.KryptonButtonBrowseVideoFile.TabIndex = 2;
            this.KryptonButtonBrowseVideoFile.Values.Text = "Browse";
            this.KryptonButtonBrowseVideoFile.Click += new System.EventHandler(this.KryptonButtonBrowse_Click);
            // 
            // KryptonTextBoxVideoFile
            // 
            this.KryptonTextBoxVideoFile.Location = new System.Drawing.Point(115, 247);
            this.KryptonTextBoxVideoFile.Name = "KryptonTextBoxVideoFile";
            this.KryptonTextBoxVideoFile.Size = new System.Drawing.Size(345, 23);
            this.KryptonTextBoxVideoFile.TabIndex = 1;
            this.KryptonTextBoxVideoFile.MouseHover += new System.EventHandler(this.KryptonTextBoxVideoFile_MouseHover);
            // 
            // PictureBoxVideo
            // 
            this.PictureBoxVideo.Location = new System.Drawing.Point(115, 58);
            this.PictureBoxVideo.Name = "PictureBoxVideo";
            this.PictureBoxVideo.Size = new System.Drawing.Size(419, 138);
            this.PictureBoxVideo.TabIndex = 0;
            this.PictureBoxVideo.TabStop = false;
            // 
            // KryptonPageWindow
            // 
            this.KryptonPageWindow.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.KryptonPageWindow.Controls.Add(this.KryptonCheckBoxInvertColourStream);
            this.KryptonPageWindow.Controls.Add(this.KryptonGroupBoxStream);
            this.KryptonPageWindow.Controls.Add(this.KryptonLabelForegroundWindowMessage);
            this.KryptonPageWindow.Controls.Add(this.PictureBoxWindowStream);
            this.KryptonPageWindow.Controls.Add(this.KryptonButtonBrowseWindows);
            this.KryptonPageWindow.Controls.Add(this.KryptonLabelWindowName);
            this.KryptonPageWindow.Controls.Add(this.KryptonTextBoxWindowName);
            this.KryptonPageWindow.Flags = 65534;
            this.KryptonPageWindow.LastVisibleSet = true;
            this.KryptonPageWindow.MinimumSize = new System.Drawing.Size(50, 50);
            this.KryptonPageWindow.Name = "KryptonPageWindow";
            this.KryptonPageWindow.Size = new System.Drawing.Size(656, 448);
            this.KryptonPageWindow.Text = "Window Stream";
            this.KryptonPageWindow.TextDescription = "";
            this.KryptonPageWindow.TextTitle = "";
            this.KryptonPageWindow.ToolTipTitle = "Page ToolTip";
            this.KryptonPageWindow.UniqueName = "B708E9ADD440465331B68F76E239DB31";
            // 
            // KryptonCheckBoxInvertColourStream
            // 
            this.KryptonCheckBoxInvertColourStream.Location = new System.Drawing.Point(281, 209);
            this.KryptonCheckBoxInvertColourStream.Name = "KryptonCheckBoxInvertColourStream";
            this.KryptonCheckBoxInvertColourStream.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonCheckBoxInvertColourStream.Size = new System.Drawing.Size(94, 20);
            this.KryptonCheckBoxInvertColourStream.TabIndex = 8;
            this.KryptonCheckBoxInvertColourStream.Values.Text = "Invert Colour";
            // 
            // KryptonGroupBoxStream
            // 
            this.KryptonGroupBoxStream.Location = new System.Drawing.Point(177, 303);
            this.KryptonGroupBoxStream.Name = "KryptonGroupBoxStream";
            this.KryptonGroupBoxStream.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxStream.Panel
            // 
            this.KryptonGroupBoxStream.Panel.Controls.Add(this.KryptonButtonStartStreaming);
            this.KryptonGroupBoxStream.Panel.Controls.Add(this.KryptonButtonStopStreaming);
            this.KryptonGroupBoxStream.Size = new System.Drawing.Size(283, 111);
            this.KryptonGroupBoxStream.TabIndex = 7;
            this.KryptonGroupBoxStream.Values.Heading = "Controls";
            // 
            // KryptonButtonStartStreaming
            // 
            this.KryptonButtonStartStreaming.Location = new System.Drawing.Point(35, 31);
            this.KryptonButtonStartStreaming.Name = "KryptonButtonStartStreaming";
            this.KryptonButtonStartStreaming.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonStartStreaming.TabIndex = 0;
            this.KryptonButtonStartStreaming.Values.Text = "Start";
            this.KryptonButtonStartStreaming.Click += new System.EventHandler(this.KryptonButtonStartStreaming_Click);
            // 
            // KryptonButtonStopStreaming
            // 
            this.KryptonButtonStopStreaming.Enabled = false;
            this.KryptonButtonStopStreaming.Location = new System.Drawing.Point(184, 31);
            this.KryptonButtonStopStreaming.Name = "KryptonButtonStopStreaming";
            this.KryptonButtonStopStreaming.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonStopStreaming.TabIndex = 1;
            this.KryptonButtonStopStreaming.Values.Text = "Stop";
            this.KryptonButtonStopStreaming.Click += new System.EventHandler(this.KryptonButtonStopStreaming_Click);
            // 
            // KryptonLabelForegroundWindowMessage
            // 
            this.KryptonLabelForegroundWindowMessage.Location = new System.Drawing.Point(230, 276);
            this.KryptonLabelForegroundWindowMessage.Name = "KryptonLabelForegroundWindowMessage";
            this.KryptonLabelForegroundWindowMessage.Size = new System.Drawing.Size(183, 20);
            this.KryptonLabelForegroundWindowMessage.TabIndex = 6;
            this.KryptonLabelForegroundWindowMessage.Values.Text = "Make sure the window is visible";
            // 
            // PictureBoxWindowStream
            // 
            this.PictureBoxWindowStream.Location = new System.Drawing.Point(115, 58);
            this.PictureBoxWindowStream.Name = "PictureBoxWindowStream";
            this.PictureBoxWindowStream.Size = new System.Drawing.Size(419, 138);
            this.PictureBoxWindowStream.TabIndex = 5;
            this.PictureBoxWindowStream.TabStop = false;
            // 
            // KryptonButtonBrowseWindows
            // 
            this.KryptonButtonBrowseWindows.Location = new System.Drawing.Point(481, 247);
            this.KryptonButtonBrowseWindows.Name = "KryptonButtonBrowseWindows";
            this.KryptonButtonBrowseWindows.Size = new System.Drawing.Size(53, 25);
            this.KryptonButtonBrowseWindows.TabIndex = 4;
            this.KryptonButtonBrowseWindows.Values.Text = "Browse";
            this.KryptonButtonBrowseWindows.Click += new System.EventHandler(this.KryptonButtonBrowseWindows_Click);
            // 
            // KryptonLabelWindowName
            // 
            this.KryptonLabelWindowName.Location = new System.Drawing.Point(14, 247);
            this.KryptonLabelWindowName.Name = "KryptonLabelWindowName";
            this.KryptonLabelWindowName.Size = new System.Drawing.Size(92, 20);
            this.KryptonLabelWindowName.TabIndex = 3;
            this.KryptonLabelWindowName.Values.Text = "Window name:";
            // 
            // KryptonTextBoxWindowName
            // 
            this.KryptonTextBoxWindowName.Location = new System.Drawing.Point(115, 247);
            this.KryptonTextBoxWindowName.Name = "KryptonTextBoxWindowName";
            this.KryptonTextBoxWindowName.Size = new System.Drawing.Size(345, 23);
            this.KryptonTextBoxWindowName.TabIndex = 2;
            // 
            // KryptonPageAreaStream
            // 
            this.KryptonPageAreaStream.AutoHiddenSlideSize = new System.Drawing.Size(200, 200);
            this.KryptonPageAreaStream.Controls.Add(this.KryptonGroupBoxArea);
            this.KryptonPageAreaStream.Controls.Add(this.KryptonCheckBoxInvertColour);
            this.KryptonPageAreaStream.Controls.Add(this.PictureBoxAreaStreaming);
            this.KryptonPageAreaStream.Flags = 65534;
            this.KryptonPageAreaStream.LastVisibleSet = true;
            this.KryptonPageAreaStream.MinimumSize = new System.Drawing.Size(50, 50);
            this.KryptonPageAreaStream.Name = "KryptonPageAreaStream";
            this.KryptonPageAreaStream.Size = new System.Drawing.Size(656, 448);
            this.KryptonPageAreaStream.Text = "Area Stream";
            this.KryptonPageAreaStream.TextDescription = "";
            this.KryptonPageAreaStream.TextTitle = "";
            this.KryptonPageAreaStream.ToolTipTitle = "Page ToolTip";
            this.KryptonPageAreaStream.UniqueName = "74117BFCE181409A208D34C6B54EB1CA";
            // 
            // KryptonGroupBoxArea
            // 
            this.KryptonGroupBoxArea.Location = new System.Drawing.Point(177, 303);
            this.KryptonGroupBoxArea.Name = "KryptonGroupBoxArea";
            this.KryptonGroupBoxArea.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            // 
            // KryptonGroupBoxArea.Panel
            // 
            this.KryptonGroupBoxArea.Panel.Controls.Add(this.KryptonButtonDoneAreaStreaming);
            this.KryptonGroupBoxArea.Panel.Controls.Add(this.KryptonButtonStartAreaStreaming);
            this.KryptonGroupBoxArea.Size = new System.Drawing.Size(283, 111);
            this.KryptonGroupBoxArea.TabIndex = 8;
            this.KryptonGroupBoxArea.Values.Heading = "Controls";
            // 
            // KryptonButtonDoneAreaStreaming
            // 
            this.KryptonButtonDoneAreaStreaming.Enabled = false;
            this.KryptonButtonDoneAreaStreaming.Location = new System.Drawing.Point(184, 31);
            this.KryptonButtonDoneAreaStreaming.Name = "KryptonButtonDoneAreaStreaming";
            this.KryptonButtonDoneAreaStreaming.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonDoneAreaStreaming.TabIndex = 2;
            this.KryptonButtonDoneAreaStreaming.Values.Text = "Done";
            this.KryptonButtonDoneAreaStreaming.Click += new System.EventHandler(this.KryptonButtonDoneAreaStreaming_Click);
            // 
            // KryptonButtonStartAreaStreaming
            // 
            this.KryptonButtonStartAreaStreaming.Location = new System.Drawing.Point(35, 31);
            this.KryptonButtonStartAreaStreaming.Name = "KryptonButtonStartAreaStreaming";
            this.KryptonButtonStartAreaStreaming.Size = new System.Drawing.Size(50, 30);
            this.KryptonButtonStartAreaStreaming.TabIndex = 1;
            this.KryptonButtonStartAreaStreaming.Values.Text = "Go";
            this.KryptonButtonStartAreaStreaming.Click += new System.EventHandler(this.KryptonButtonStartAreaStreaming_Click);
            // 
            // KryptonCheckBoxInvertColour
            // 
            this.KryptonCheckBoxInvertColour.Location = new System.Drawing.Point(281, 209);
            this.KryptonCheckBoxInvertColour.Name = "KryptonCheckBoxInvertColour";
            this.KryptonCheckBoxInvertColour.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2010Silver;
            this.KryptonCheckBoxInvertColour.Size = new System.Drawing.Size(94, 20);
            this.KryptonCheckBoxInvertColour.TabIndex = 7;
            this.KryptonCheckBoxInvertColour.Values.Text = "Invert Colour";
            // 
            // PictureBoxAreaStreaming
            // 
            this.PictureBoxAreaStreaming.Location = new System.Drawing.Point(115, 58);
            this.PictureBoxAreaStreaming.Name = "PictureBoxAreaStreaming";
            this.PictureBoxAreaStreaming.Size = new System.Drawing.Size(419, 138);
            this.PictureBoxAreaStreaming.TabIndex = 6;
            this.PictureBoxAreaStreaming.TabStop = false;
            // 
            // TimerWindowStreaming
            // 
            this.TimerWindowStreaming.Interval = 15;
            this.TimerWindowStreaming.Tick += new System.EventHandler(this.TimerStreaming_Tick);
            // 
            // TimerAreaStreaming
            // 
            this.TimerAreaStreaming.Interval = 15;
            this.TimerAreaStreaming.Tick += new System.EventHandler(this.TimerAreaStreaming_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.KryptonNavigator);
            this.Name = "MainForm";
            this.Text = "Stream On VisionAR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonNavigator)).EndInit();
            this.KryptonNavigator.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageVideo)).EndInit();
            this.KryptonPageVideo.ResumeLayout(false);
            this.KryptonPageVideo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVideo.Panel)).EndInit();
            this.KryptonGroupBoxVideo.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxVideo)).EndInit();
            this.KryptonGroupBoxVideo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxVideo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageWindow)).EndInit();
            this.KryptonPageWindow.ResumeLayout(false);
            this.KryptonPageWindow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxStream.Panel)).EndInit();
            this.KryptonGroupBoxStream.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxStream)).EndInit();
            this.KryptonGroupBoxStream.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxWindowStream)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonPageAreaStream)).EndInit();
            this.KryptonPageAreaStream.ResumeLayout(false);
            this.KryptonPageAreaStream.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxArea.Panel)).EndInit();
            this.KryptonGroupBoxArea.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.KryptonGroupBoxArea)).EndInit();
            this.KryptonGroupBoxArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAreaStreaming)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Navigator.KryptonNavigator KryptonNavigator;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageVideo;
        private System.Windows.Forms.PictureBox PictureBoxVideo;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageWindow;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonBrowseVideoFile;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox KryptonTextBoxVideoFile;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonPlayPause;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStop;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStartStreaming;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStopStreaming;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelVideoName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox KryptonTextBoxWindowName;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelWindowName;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonBrowseWindows;
        private System.Windows.Forms.PictureBox PictureBoxWindowStream;
        private System.Windows.Forms.Timer TimerWindowStreaming;
        private ComponentFactory.Krypton.Navigator.KryptonPage KryptonPageAreaStream;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonStartAreaStreaming;
        private System.Windows.Forms.Timer TimerAreaStreaming;
        private ComponentFactory.Krypton.Toolkit.KryptonButton KryptonButtonDoneAreaStreaming;
        private System.Windows.Forms.PictureBox PictureBoxAreaStreaming;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel KryptonLabelForegroundWindowMessage;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox KryptonCheckBoxInvertColour;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxVideo;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxStream;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox KryptonCheckBoxInvertColourVideo;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckBox KryptonCheckBoxInvertColourStream;
        private ComponentFactory.Krypton.Toolkit.KryptonGroupBox KryptonGroupBoxArea;
    }
}

