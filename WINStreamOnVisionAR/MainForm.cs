﻿using Accord.Video.FFMPEG;
using ComponentFactory.Krypton.Toolkit;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace WINStreamOnVisionAR
{
    public partial class MainForm : KryptonForm
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", EntryPoint = "GetWindowText", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpWindowText, int nMaxCount);

        [DllImport("user32.dll", EntryPoint = "EnumDesktopWindows", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool EnumDesktopWindows(IntPtr hDesktop, EnumDelegate lpEnumCallbackFunction, IntPtr lParam);

        // Define the callback delegate's type.
        private delegate bool EnumDelegate(IntPtr hWnd, int lParam);

        private class User32
        {
            [StructLayout(LayoutKind.Sequential)]
            public struct RECT
            {
                public int left;
                public int top;
                public int right;
                public int bottom;
            }

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowDC(IntPtr hWnd);

            [DllImport("user32.dll")]
            public static extern IntPtr ReleaseDC(IntPtr hWnd, IntPtr hDC);

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd, ref RECT rect);

            [DllImport("user32.dll")]
            public static extern IntPtr FindWindow(string className, string windowTitle);
        }

        private class GDI32
        {
            public const int SRCCOPY = 0x00CC0020;  // BitBlt dwRop parameter

            [DllImport("gdi32.dll")]
            public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest,
                int nWidth, int nHeight, IntPtr hObjectSource,
                int nXSrc, int nYSrc, int dwRop);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleBitmap(IntPtr hDC, int nWidth, int nHeight);

            [DllImport("gdi32.dll")]
            public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteDC(IntPtr hDC);

            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);

            [DllImport("gdi32.dll")]
            public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        }

        private class VisionARDLL
        {
            [DllImport("VisionARFB.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool Startup();

            [DllImport("VisionARCDC.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool StartupCdc();

            [DllImport("VisionARFB.dll", EntryPoint = "CloseFb", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevFb();

            [DllImport("VisionARCDC.dll", EntryPoint = "CloseCdc", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevCdc();

            [DllImport("VisionARFB.dll", EntryPoint = "WriteImg", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool WriteImg(byte[] data);
        }

        const int WIDTH = 419, HEIGTH = 138;

        private bool isVideoPlaying = false, isVideoPaused = false, isWindowStreaming = false, isAreaStreaming = false;

        private Thread tVideoStream;

        private static HashSet<string> windowTitles;

        private AreaSelectionForm areaSelectionForm;


        public MainForm()
        {
            InitializeComponent();

            if (!VisionARDLL.Startup() || !VisionARDLL.StartupCdc())
            {
                MessageBox.Show("Please make sure the glasses are connected", "Glasses not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!VisionARDLL.CloseDevFb()) Console.WriteLine("error devicefb close");
            if (!VisionARDLL.CloseDevCdc()) Console.WriteLine("error devicecdc close");

            if (isVideoPlaying || isVideoPaused)
            {
                KryptonButtonStop_Click(sender, e);
            }

            if (isWindowStreaming)
            {
                KryptonButtonStopStreaming_Click(sender, e);
            }

            if (isAreaStreaming)
            {
                KryptonButtonDoneAreaStreaming_Click(sender, e);
            }
        }


        private static byte[] BitmapToByteArray(Bitmap img)
        {
            Rectangle rect = new Rectangle(0, 0, img.Width, img.Height);
            BitmapData bitmapData = img.LockBits(rect, ImageLockMode.ReadOnly, img.PixelFormat);
            int length = bitmapData.Stride * bitmapData.Height;

            byte[] bytes = new byte[length];

            Marshal.Copy(bitmapData.Scan0, bytes, 0, length);
            img.UnlockBits(bitmapData);
            return bytes;
        }


        private void KryptonButtonBrowse_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                fileDialog.Filter = "All files (*.*)|*.*|mp4 files (*.mp4)|*.mp4";
                fileDialog.FilterIndex = 2;
                fileDialog.RestoreDirectory = true;
                fileDialog.Multiselect = false;

                if (fileDialog.ShowDialog() == DialogResult.OK)
                {
                    KryptonTextBoxVideoFile.Text = fileDialog.FileName;
                }
            }
        }


        private void KryptonButtonPlayPause_Click(object sender, EventArgs e)
        {
            if (isVideoPlaying)  // pause click
            {
                tVideoStream.Suspend();

                KryptonButtonPlayPause.Text = "Play";

                isVideoPaused = true;
                isVideoPlaying = false;
            }
            else  // play click
            {
                if (isVideoPaused)
                {
                    tVideoStream.Resume();
                }
                else
                {
                    if (ValidateInputVideoFile())
                    {
                        tVideoStream = new Thread(() => PlayFromFile());
                        tVideoStream.Start();
                    }
                    else
                    {
                        return;
                    }
                }

                KryptonButtonPlayPause.Text = "Pause";

                KryptonTextBoxVideoFile.Enabled = false;
                KryptonButtonBrowseVideoFile.Enabled = false;
                KryptonButtonStop.Enabled = true;

                KryptonPageWindow.Enabled = false;
                KryptonPageAreaStream.Enabled = false;

                isVideoPaused = false;
                isVideoPlaying = true;
            }
        }


        private void KryptonButtonStop_Click(object sender, EventArgs e)
        {
            if (isVideoPlaying || isVideoPaused)
            {
                if (isVideoPaused) tVideoStream.Resume();
                
                tVideoStream.Abort();

                KryptonButtonPlayPause.Text = "Play";

                KryptonTextBoxVideoFile.Enabled = true;
                KryptonButtonBrowseVideoFile.Enabled = true;
                KryptonButtonStop.Enabled = false;

                KryptonPageWindow.Enabled = true;
                KryptonPageAreaStream.Enabled = true;

                isVideoPaused = false;
                isVideoPlaying = false;

                PictureBoxVideo.Image = null;
            }
        }


        private bool ValidateInputVideoFile()
        {
            if (string.IsNullOrWhiteSpace(KryptonTextBoxVideoFile.Text))
            {
                return false;
            }

            if (!KryptonTextBoxVideoFile.Text.EndsWith(".mp4"))
            {
                return false;
            }

            return File.Exists(KryptonTextBoxVideoFile.Text);
        }


        private void PlayFromFile()
        {
            using (VideoFileReader fileReader = new VideoFileReader())
            {
                try 
                {
                    fileReader.Open(KryptonTextBoxVideoFile.Text);
                    
                    for (long i = 0; i < fileReader.FrameCount; i++)
                    {
                        Bitmap frame = fileReader.ReadVideoFrame();
                        
                        if (KryptonCheckBoxInvertColourVideo.Checked)
                        {
                            frame = InvertImage(frame);
                        }

                        if (!VisionARDLL.WriteImg(BitmapToByteArray(new Bitmap(frame, WIDTH, HEIGTH))))
                        {
                            Console.WriteLine("error WriteImg");
                        }

                        PictureBoxVideo.Image = new Bitmap(frame, PictureBoxVideo.Width, PictureBoxVideo.Height);

                        frame.Dispose();
                    }

                    fileReader.Close();
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }


        private void KryptonTextBoxVideoFile_MouseHover(object sender, EventArgs e)
        {
            ToolTip TT = new ToolTip();
            TT.Show(KryptonTextBoxVideoFile.Text, KryptonTextBoxVideoFile, 0, 0, 2500);
        }


        private void KryptonButtonStartStreaming_Click(object sender, EventArgs e)
        {
            if (!isWindowStreaming && !string.IsNullOrWhiteSpace(KryptonTextBoxWindowName.Text))
            {
                KryptonButtonBrowseWindows.Enabled = false;
                KryptonButtonStartStreaming.Enabled = false;
                KryptonButtonStopStreaming.Enabled = true;
                KryptonTextBoxWindowName.Enabled = false;

                KryptonPageVideo.Enabled = false;
                KryptonPageAreaStream.Enabled = false;

                isWindowStreaming = true;

                TimerWindowStreaming.Start();
            }
        }


        private void KryptonButtonStopStreaming_Click(object sender, EventArgs e)
        {
            if (isWindowStreaming)
            {
                KryptonButtonBrowseWindows.Enabled = true;
                KryptonButtonStartStreaming.Enabled = true;
                KryptonButtonStopStreaming.Enabled = false;
                KryptonTextBoxWindowName.Enabled = true;

                KryptonPageVideo.Enabled = true;
                KryptonPageAreaStream.Enabled = true;

                isWindowStreaming = false;

                TimerWindowStreaming.Stop();

                PictureBoxWindowStream.Image = null;
            }
        }


        private void KryptonButtonBrowseWindows_Click(object sender, EventArgs e)
        {
            GetDesktopWindowTitles(out HashSet<string> windows);

            WindowSelectionForm form = new WindowSelectionForm(windows);
            form.ShowDialog();

            KryptonTextBoxWindowName.Text = form.selectedText;
        }


        private void TimerStreaming_Tick(object sender, EventArgs e)
        {
            CaptureWindowFrame();
        }


        private void KryptonButtonStartAreaStreaming_Click(object sender, EventArgs e)
        {
            if (!isAreaStreaming)
            {
                areaSelectionForm = new AreaSelectionForm(this);

                areaSelectionForm.Show();
                TimerAreaStreaming.Start();

                KryptonButtonStartAreaStreaming.Enabled = false;
                KryptonButtonDoneAreaStreaming.Enabled = true;

                KryptonPageVideo.Enabled = false;
                KryptonPageWindow.Enabled = false;

                isAreaStreaming = true;
            }
        }


        private void TimerAreaStreaming_Tick(object sender, EventArgs e)
        {
            TimerAreaStreaming.Enabled = false;

            if (areaSelectionForm != null && areaSelectionForm.Visible)
            {
                MemoryStream ms = areaSelectionForm.GetSelected();

                Bitmap bmp = new Bitmap(Image.FromStream(ms));
                
                if (KryptonCheckBoxInvertColour.Checked)
                {
                    bmp = InvertImage(bmp);
                }

                if (!VisionARDLL.WriteImg(BitmapToByteArray(bmp)))
                {
                    Console.WriteLine("error WriteImg");
                }

                PictureBoxAreaStreaming.Image = new Bitmap(bmp);

                bmp.Dispose();
            }

            TimerAreaStreaming.Enabled = true;
        }


        private void KryptonButtonDoneAreaStreaming_Click(object sender, EventArgs e)
        {
            if (isAreaStreaming)
            {
                areaSelectionForm.Close();

                KryptonButtonStartAreaStreaming.Enabled = true;
                KryptonButtonDoneAreaStreaming.Enabled = false;

                KryptonPageVideo.Enabled = true;
                KryptonPageWindow.Enabled = true;

                isAreaStreaming = false;
            }
        }


        private static Bitmap InvertImage(Bitmap source)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(source.Width, source.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            // create the negative color matrix
            ColorMatrix colorMatrix = new ColorMatrix(new float[][]
            {
                new float[] {-1, 0, 0, 0, 0},
                new float[] {0, -1, 0, 0, 0},
                new float[] {0, 0, -1, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {1, 1, 1, 0, 1}
            });

            // create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            attributes.SetColorMatrix(colorMatrix);

            g.DrawImage(source, new Rectangle(0, 0, source.Width, source.Height),
                        0, 0, source.Width, source.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();

            return newBitmap;
        }


        public void AreaSelectionClosing()
        {
            TimerAreaStreaming.Stop();
            areaSelectionForm = null;

            Bitmap bmp = new Bitmap(WIDTH, HEIGTH, PixelFormat.Format32bppArgb);
            
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.FillRectangle(Brushes.Black, 0, 0, WIDTH, HEIGTH);

                if (!VisionARDLL.WriteImg(BitmapToByteArray(bmp)))
                {
                    Console.WriteLine("error WriteImg");
                }

                PictureBoxAreaStreaming.Image = null;
            }

            bmp.Dispose();
        }


        private void CaptureWindowFrame()
        {
            IntPtr handle = User32.FindWindow(null, KryptonTextBoxWindowName.Text);

            // get te hDC of the target window
            IntPtr hdcSrc = User32.GetWindowDC(handle);
            // get the size
            User32.RECT windowRect = new User32.RECT();
            User32.GetWindowRect(handle, ref windowRect);
            int width = windowRect.right - windowRect.left;
            int height = windowRect.bottom - windowRect.top;
            // create a device context we can copy to
            IntPtr hdcDest = GDI32.CreateCompatibleDC(hdcSrc);
            // create a bitmap we can copy it to, using GetDeviceCaps to get the width/height
            IntPtr hBitmap = GDI32.CreateCompatibleBitmap(hdcSrc, width, height);
            // select the bitmap object
            IntPtr hOld = GDI32.SelectObject(hdcDest, hBitmap);
            // bitblt over
            GDI32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, 0, 0, GDI32.SRCCOPY);
            // restore selection
            GDI32.SelectObject(hdcDest, hOld);
            // clean up
            GDI32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle, hdcSrc);
            // get a .NET image object for it
            Bitmap bmp = Image.FromHbitmap(hBitmap);
            // free up the Bitmap object
            GDI32.DeleteObject(hBitmap);

            Bitmap frame = new Bitmap(bmp, WIDTH, HEIGTH);

            if (KryptonCheckBoxInvertColourStream.Checked)
            {
                frame = InvertImage(frame);
            }

            if (!VisionARDLL.WriteImg(BitmapToByteArray(frame)))
            {
                Console.WriteLine("error WriteImg");
            }

            PictureBoxWindowStream.Image = new Bitmap(frame, PictureBoxWindowStream.Width, PictureBoxWindowStream.Height);

            frame.Dispose();
            bmp.Dispose();
        }


        public static void GetDesktopWindowTitles(out HashSet<string> titles)
        {
            windowTitles = new HashSet<string>();

            if (EnumDesktopWindows(IntPtr.Zero, FilterCallback, IntPtr.Zero))
            {
                titles = windowTitles;
            }
            else
            {
                titles = null;
            }
        }


        private static bool FilterCallback(IntPtr hWnd, int lParam)
        {
            StringBuilder sb_title = new StringBuilder(1024);

            GetWindowText(hWnd, sb_title, sb_title.Capacity);

            string title = sb_title.ToString();

            if (IsWindowVisible(hWnd) && !string.IsNullOrEmpty(title))
            {
                windowTitles.Add(title);
            }

            return true;
        }
    }
}
