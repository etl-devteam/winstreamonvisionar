﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace WINStreamOnVisionAR
{
    public partial class AreaSelectionForm : Form
    {
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);


        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        private readonly MainForm mainForm;


        public AreaSelectionForm(MainForm form)
        {
            InitializeComponent();

            mainForm = form;
        }



        private void AreaSelectionForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Cursor.Current = Cursors.Hand;

                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }


        private void AreaSelectionForm_MouseLeave(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.Default;
        }


        public MemoryStream GetSelected()
        {
            Rectangle rect = new Rectangle(Location.X, Location.Y, Width, Height);

            using (Bitmap bmp = new Bitmap(Width, Height, PixelFormat.Format32bppArgb))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.CopyFromScreen(rect.Left, rect.Top, 0, 0, Size, CopyPixelOperation.SourceCopy);

                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, ImageFormat.Jpeg);

                    return ms;
                }
            }
        }


        private void AreaSelectionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            mainForm.AreaSelectionClosing();
        }
    }
}
