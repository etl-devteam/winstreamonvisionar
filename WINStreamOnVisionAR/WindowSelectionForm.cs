﻿using System.Collections.Generic;
using System.Windows.Forms;


namespace WINStreamOnVisionAR
{
    public partial class WindowSelectionForm : Form
    {
        internal string selectedText;


        public WindowSelectionForm(HashSet<string> windows)
        {
            InitializeComponent();

            foreach (string window in windows)
            {
                KryptonListBoxWindows.Items.Add(window);
            }
        }


        private void KryptonListBoxWindows_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            selectedText = KryptonListBoxWindows.SelectedItem.ToString();

            Close();
        }
    }
}
