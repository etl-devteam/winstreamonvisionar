; CheckAndDownloadDotNet.nsi
;
; It will install "Hello, world" application for Windows into a directory that the user selects,
; creating start menu and desktop links, according to user choice.

!pragma warning disable 6010

;--------------------------------
Function CheckAndDownloadDotNet
# Let's see if the user has the .NET Framework installed, according to $0 version on their
# system or not.
# Remember: you need Vista SP2 or 7 SP1.  It is built in to Windows 8, and not needed
# In case you're wondering, running this code on Windows 8 will correctly return is_equal
# or is_greater (maybe Microsoft releases .NET 4.5 SP1 for example)

pop $0 ; .NET release number
pop $1 ; installer URL

# Set up our Variables
Var /GLOBAL dotNETIsThere
Var /GLOBAL dotNET_CMD_LINE
Var /GLOBAL EXIT_CODE

ReadRegDWORD $dotNETIsThere HKLM "SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full" "Release"

IntCmp $dotNETIsThere $0 is_equal is_less is_greater

is_equal:
    Goto done_compare_not_needed
is_greater:
    # Useful if, for example, Microsoft releases .NET 4.5 SP1
    # We want to be able to simply skip install since it's not
    # needed on this system
    Goto done_compare_not_needed
is_less:
    Goto done_compare_needed

done_compare_needed:
    #.NET Framework install is *NEEDED*

    # Setup looks for installer
    # This allows the installer to be placed on a USB stick (for computers without internet connections)
    # If the .NET Framework 4.5 installer is *NOT* found, Setup will connect to Microsoft's website
    # and download it for you

    # Reboot Required with these Exit Codes:
    # 1641 or 3010

    # Command Line Switches:
    # /showrmui /passive /norestart

    # Silent Command Line Switches:
    # /q /norestart


    # Let's see if the user is doing a Silent install or not
    IfSilent is_quiet is_not_quiet

    is_quiet:
        StrCpy $dotNET_CMD_LINE "/q /norestart"
        Goto do_network_install
    is_not_quiet:
        StrCpy $dotNET_CMD_LINE "/showrmui /passive /norestart"
        Goto do_network_install

    # Now, let's Download the .NET
    do_network_install:
        inetc::get $1 "$TEMP\dotNET.exe"
	    Pop $R0
        StrCmp $R0 "OK" success
            MessageBox MB_OK|MB_ICONEXCLAMATION "Unable to download .NET Framework. Product will be installed, but will not function without the Framework!"
            Goto done_dotNET_function

	success:
        ExecWait '"$TEMP\dotNET.exe" $dotNET_CMD_LINE' $EXIT_CODE
        # $EXIT_CODE contains the return codes.  1641 and 3010 means a Reboot has been requested
        IntCmp $EXIT_CODE 1641 reboot_required
        IntCmp $EXIT_CODE 3010 reboot_required
        Goto done_dotNET_function

    reboot_required:
        SetRebootFlag true

done_compare_not_needed:
    # Done dotNET Install
    Goto done_dotNET_function

#exit the function
done_dotNET_function:

FunctionEnd


Function CheckAndDownloadDotNet451

Push https://go.microsoft.com/fwlink/?LinkId=397708
Push 378675
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet452

Push https://go.microsoft.com/fwlink/?LinkId=322116
Push 379893
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet46

Push https://go.microsoft.com/fwlink/?LinkId=2099384
Push 393295
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet461

Push https://go.microsoft.com/fwlink/?LinkId=2099467
Push 394254
Call CheckAndDownloadDotNet

FunctionEnd

Function CheckAndDownloadDotNet462

Push https://go.microsoft.com/fwlink/?linkid=2099468
Push 394802
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet47

Push https://go.microsoft.com/fwlink/?LinkId=2099385
Push 460798
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet471

Push https://go.microsoft.com/fwlink/?LinkID=2099383
Push 461308
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet472

Push https://go.microsoft.com/fwlink/?LinkID=863265
Push 461808
Call CheckAndDownloadDotNet

FunctionEnd


Function CheckAndDownloadDotNet48

Push https://go.microsoft.com/fwlink/?linkid=2088631
Push 528040
Call CheckAndDownloadDotNet

FunctionEnd
