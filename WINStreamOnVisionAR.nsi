; WINStreamOnVisionAR.nsi
;
; It will install "WINStreamOnVisionAR" application for Windows into a directory that the user selects,
; creating start menu and desktop links, according to user choice.

;--------------------------------


; The name of the installer
Name "VisionAR Stream On"

; The file to write
OutFile "visionarstreamon.exe"

; Installer Icon
Icon "VisionAR.ico"

; Request application privileges for Windows Vista and higher
RequestExecutionLevel admin

; Build Unicode installer
Unicode True

; The default installation directory
InstallDir "C:\ETL\VisionARStreamOn"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\VisionARStreamOn" "Install_Dir"

;--------------------------------

!include CheckAndDownloadVCRedist.nsh
!include CheckAndDownloaddotnet.nsh

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "VisionAR Stream On (required)"

  SectionIn RO
  
  Call CheckAndDownloadVCRedist2015
  Call CheckAndDownloadDotNet472

  CreateDirectory "$INSTDIR"
  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  
  ; Put file there
  File "WINStreamOnVisionAR\bin\Release\Accord.dll.config"
  File "WINStreamOnVisionAR\bin\Release\WINStreamOnVisionAR.exe.config"
  File "WINStreamOnVisionAR\bin\Release\Accord.dll"
  File "WINStreamOnVisionAR\bin\Release\Accord.Video.dll"
  File "WINStreamOnVisionAR\bin\Release\Accord.Video.FFMPEG.x64.dll"
  File "WINStreamOnVisionAR\bin\Release\avcodec-57.dll"
  File "WINStreamOnVisionAR\bin\Release\avdevice-57.dll"
  File "WINStreamOnVisionAR\bin\Release\avfilter-6.dll"
  File "WINStreamOnVisionAR\bin\Release\avformat-57.dll"
  File "WINStreamOnVisionAR\bin\Release\avutil-55.dll"
  File "WINStreamOnVisionAR\bin\Release\VisionARCDC.dll"
  File "WINStreamOnVisionAR\bin\Release\VisionARFB.dll"
  File "WINStreamOnVisionAR\bin\Release\postproc-54.dll"
  File "WINStreamOnVisionAR\bin\Release\swresample-2.dll"
  File "WINStreamOnVisionAR\bin\Release\swscale-4.dll"
  File "WINStreamOnVisionAR\bin\Release\WINStreamOnVisionAR.exe"
  File "WINStreamOnVisionAR\bin\Release\Krypton Docking.dll"
  File "WINStreamOnVisionAR\bin\Release\Krypton Navigator.dll"
  File "WINStreamOnVisionAR\bin\Release\Krypton Ribbon.dll"
  File "WINStreamOnVisionAR\bin\Release\Krypton Toolkit.dll"
  File "WINStreamOnVisionAR\bin\Release\Krypton Workspace.dll"
  File "VisionAR.ico"

  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\VisionARStreamOn "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "DisplayName" "VisionAR Stream On"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "InstallLocation" '"$INSTDIR"'
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "Publisher" "Univet s.r.l."
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "URLInfoAbout" "www.univetar.com"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "DisplayVersion" "3.2"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "VersionMajor" 3
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "VersionMinor" 2
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "NoRepair" 1
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn" "DisplayIcon" "$INSTDIR\VisionAR.ico,0"


  WriteUninstaller "$INSTDIR\uninstall.exe"
  
SectionEnd


; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"

  CreateDirectory "$SMPROGRAMS\VisionARStreamOn"
  CreateShortcut "$SMPROGRAMS\VisionARStreamOn\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\VisionARStreamOn\VisionAR Stream On.lnk" "$INSTDIR\WINStreamOnVisionAR.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

; Optional section (can be disabled by the user)
Section "Desktop Shortcut"

  ; Set shortcut working path to the installation directory.
  SetOutPath "$INSTDIR"
  
  CreateShortcut "$DESKTOP\VisionAR Stream On.lnk" "$INSTDIR\WINStreamOnVisionAR.exe" "" "$INSTDIR\VisionAR.ico"

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\VisionARStreamOn"
  DeleteRegKey HKLM SOFTWARE\VisionARStreamOn

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\VisionAR Stream On\*.lnk"
  Delete "$DESKTOP\VisionAR Stream On.lnk"

  ; Remove files and uninstaller
  Delete "$INSTDIR\Accord.dll.config"
  Delete "$INSTDIR\WINStreamOnVisionAR.exe.config"
  Delete "$INSTDIR\Accord.dll"
  Delete "$INSTDIR\Accord.Video.dll"
  Delete "$INSTDIR\Accord.Video.FFMPEG.x64.dll"
  Delete "$INSTDIR\avcodec-57.dll"
  Delete "$INSTDIR\avdevice-57.dll"
  Delete "$INSTDIR\avfilter-6.dll"
  Delete "$INSTDIR\avformat-57.dll"
  Delete "$INSTDIR\avutil-55.dll"
  Delete "$INSTDIR\VisionARCDC.dll"
  Delete "$INSTDIR\VisionARFB.dll"
  Delete "$INSTDIR\postproc-54.dll"
  Delete "$INSTDIR\swresample-2.dll"
  Delete "$INSTDIR\swscale-4.dll"
  Delete "$INSTDIR\WINStreamOnVisionAR.exe"
  Delete "$INSTDIR\VisionAR.ico"
  Delete "$INSTDIR\Krypton Docking.dll"
  Delete "$INSTDIR\Krypton Navigator.dll"
  Delete "$INSTDIR\Krypton Ribbon.dll"
  Delete "$INSTDIR\Krypton Toolkit.dll"
  Delete "$INSTDIR\Krypton Workspace.dll"
  Delete "$INSTDIR\uninstall.exe"

  ; Remove directories
  RMDir "$SMPROGRAMS\VisionARStreamOn"
  RMDir /r "$INSTDIR"

SectionEnd
